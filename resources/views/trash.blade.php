<div class="OutlineElement Ltr  BCX0 SCXW166459482"
    style="margin: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow: visible; cursor: text; clear: both; position: relative; direction: ltr;">
    <h4
        style="text-align: justify; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow-wrap: break-word; vertical-align: baseline;">
        <section data-id="a7664e8"
            class="elementor-element elementor-element-a7664e8 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
            data-element_type="section" style="box-sizing: border-box; position: relative; font-family: " open=""
            sans",="" arial,="" helvetica,="" sans-serif;="" font-size:="" 14px;="" text-align:="" start;"="">
            <div class="elementor-container elementor-column-gap-default"
                style="background: transparent; border: 0px; margin: 0px auto; padding: 0px; vertical-align: baseline; box-sizing: border-box; display: flex; position: relative; max-width: 1140px;">
                <div class="elementor-row"
                    style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; width: 1140px; display: flex;">
                    <div data-id="7da477d"
                        class="elementor-element elementor-element-7da477d elementor-column elementor-col-16 elementor-top-column"
                        data-element_type="column"
                        style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; position: relative; min-height: 1px; display: flex; width: 189.984px;">
                        <div class="elementor-column-wrap elementor-element-populated"
                            style="background: transparent; border: 0px; margin: 0px; padding: 10px; vertical-align: baseline; box-sizing: border-box; display: flex; width: 189.984px; position: relative;">
                            <div class="elementor-widget-wrap"
                                style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; width: 169.984px; position: relative;">
                                <div data-id="a17ff24"
                                    class="elementor-element elementor-element-a17ff24 elementor-widget elementor-widget-image"
                                    data-element_type="image.default"
                                    style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; position: relative; text-align: center;">
                                    <div class="elementor-widget-container"
                                        style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; transition: background 0.3s ease 0s, border 0.3s ease 0s, border-radius 0.3s ease 0s, box-shadow 0.3s ease 0s, -webkit-border-radius 0.3s ease 0s, -webkit-box-shadow 0.3s ease 0s;">
                                        <div class="elementor-image"
                                            style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box;">
                                            <img width="200" height="200"
                                                src="http://medlifeao.md/wp-content/uploads/2018/11/cnam.jpg"
                                                class="attachment-full size-full" alt=""
                                                srcset="http://medlifeao.md/wp-content/uploads/2018/11/cnam.jpg 200w, http://medlifeao.md/wp-content/uploads/2018/11/cnam-150x150.jpg 150w"
                                                sizes="(max-width: 200px) 100vw, 200px"
                                                style="background: transparent; border-width: initial; border-color: initial; border-image: initial; margin: 0px; padding: 0px; max-width: 100%; line-height: 0; height: auto; box-sizing: border-box; border-radius: 0px; box-shadow: none; display: inline-block;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-id="8266e2c"
                        class="elementor-element elementor-element-8266e2c elementor-column elementor-col-16 elementor-top-column"
                        data-element_type="column"
                        style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; position: relative; min-height: 1px; display: flex; width: 189.984px;">
                        <div class="elementor-column-wrap elementor-element-populated"
                            style="background: transparent; border: 0px; margin: 0px; padding: 10px; vertical-align: baseline; box-sizing: border-box; display: flex; width: 189.984px; position: relative;">
                            <div class="elementor-widget-wrap"
                                style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; width: 169.984px; position: relative;">
                                <div data-id="72aa5a9"
                                    class="elementor-element elementor-element-72aa5a9 elementor-widget elementor-widget-image"
                                    data-element_type="image.default"
                                    style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; position: relative; text-align: center;">
                                    <div class="elementor-widget-container"
                                        style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; transition: background 0.3s ease 0s, border 0.3s ease 0s, border-radius 0.3s ease 0s, box-shadow 0.3s ease 0s, -webkit-border-radius 0.3s ease 0s, -webkit-box-shadow 0.3s ease 0s;">
                                        <div class="elementor-image"
                                            style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box;">
                                            <img width="200" height="200"
                                                src="http://medlifeao.md/wp-content/uploads/2018/11/Emblema_Guvernului_Republicii_Moldova.jpg"
                                                class="attachment-full size-full" alt=""
                                                srcset="http://medlifeao.md/wp-content/uploads/2018/11/Emblema_Guvernului_Republicii_Moldova.jpg 200w, http://medlifeao.md/wp-content/uploads/2018/11/Emblema_Guvernului_Republicii_Moldova-150x150.jpg 150w"
                                                sizes="(max-width: 200px) 100vw, 200px"
                                                style="background: transparent; border-width: initial; border-color: initial; border-image: initial; margin: 0px; padding: 0px; max-width: 100%; line-height: 0; height: auto; box-sizing: border-box; border-radius: 0px; box-shadow: none; display: inline-block;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-id="11e403f"
                        class="elementor-element elementor-element-11e403f elementor-column elementor-col-16 elementor-top-column"
                        data-element_type="column"
                        style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; position: relative; min-height: 1px; display: flex; width: 189.984px;">
                        <div class="elementor-column-wrap elementor-element-populated"
                            style="background: transparent; border: 0px; margin: 0px; padding: 10px; vertical-align: baseline; box-sizing: border-box; display: flex; width: 189.984px; position: relative;">
                            <div class="elementor-widget-wrap"
                                style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; width: 169.984px; position: relative;">
                                <div data-id="6de227b"
                                    class="elementor-element elementor-element-6de227b elementor-widget elementor-widget-image"
                                    data-element_type="image.default"
                                    style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; position: relative;">
                                    <div class="elementor-widget-container"
                                        style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; transition: background 0.3s ease 0s, border 0.3s ease 0s, border-radius 0.3s ease 0s, box-shadow 0.3s ease 0s, -webkit-border-radius 0.3s ease 0s, -webkit-box-shadow 0.3s ease 0s;">
                                        <div class="elementor-image"
                                            style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box;">
                                            <img width="200" height="200"
                                                src="http://medlifeao.md/wp-content/uploads/2018/11/balti.jpg"
                                                class="attachment-full size-full" alt=""
                                                srcset="http://medlifeao.md/wp-content/uploads/2018/11/balti.jpg 200w, http://medlifeao.md/wp-content/uploads/2018/11/balti-150x150.jpg 150w"
                                                sizes="(max-width: 200px) 100vw, 200px"
                                                style="background: transparent; border-width: initial; border-color: initial; border-image: initial; margin: 0px; padding: 0px; max-width: 100%; line-height: 0; height: auto; box-sizing: border-box; border-radius: 0px; box-shadow: none; display: inline-block; width: 169.984px; transition-duration: 0s;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-id="b75bbda"
                        class="elementor-element elementor-element-b75bbda elementor-column elementor-col-16 elementor-top-column"
                        data-element_type="column"
                        style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; position: relative; min-height: 1px; display: flex; width: 189.984px;">
                        <div class="elementor-column-wrap elementor-element-populated"
                            style="background: transparent; border: 0px; margin: 0px; padding: 10px; vertical-align: baseline; box-sizing: border-box; display: flex; width: 189.984px; position: relative;">
                            <div class="elementor-widget-wrap"
                                style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; width: 169.984px; position: relative;">
                                <div data-id="2b9d5fb"
                                    class="elementor-element elementor-element-2b9d5fb elementor-widget elementor-widget-image"
                                    data-element_type="image.default"
                                    style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; position: relative; text-align: center;">
                                    <div class="elementor-widget-container"
                                        style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; transition: background 0.3s ease 0s, border 0.3s ease 0s, border-radius 0.3s ease 0s, box-shadow 0.3s ease 0s, -webkit-border-radius 0.3s ease 0s, -webkit-box-shadow 0.3s ease 0s;">
                                        <div class="elementor-image"
                                            style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box;">
                                            <img width="200" height="200"
                                                src="http://medlifeao.md/wp-content/uploads/2018/11/col-med.jpg"
                                                class="attachment-full size-full" alt=""
                                                srcset="http://medlifeao.md/wp-content/uploads/2018/11/col-med.jpg 200w, http://medlifeao.md/wp-content/uploads/2018/11/col-med-150x150.jpg 150w"
                                                sizes="(max-width: 200px) 100vw, 200px"
                                                style="background: transparent; border-width: initial; border-color: initial; border-image: initial; margin: 0px; padding: 0px; max-width: 100%; line-height: 0; height: auto; box-sizing: border-box; border-radius: 0px; box-shadow: none; display: inline-block;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-id="bb08672"
                        class="elementor-element elementor-element-bb08672 elementor-column elementor-col-16 elementor-top-column"
                        data-element_type="column"
                        style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; position: relative; min-height: 1px; display: flex; width: 189.984px;">
                        <div class="elementor-column-wrap elementor-element-populated"
                            style="background: transparent; border: 0px; margin: 0px; padding: 10px; vertical-align: baseline; box-sizing: border-box; display: flex; width: 189.984px; position: relative;">
                            <div class="elementor-widget-wrap"
                                style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; width: 169.984px; position: relative;">
                                <div data-id="ac68ee6"
                                    class="elementor-element elementor-element-ac68ee6 elementor-widget elementor-widget-image"
                                    data-element_type="image.default"
                                    style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; position: relative; text-align: center;">
                                    <div class="elementor-widget-container"
                                        style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; transition: background 0.3s ease 0s, border 0.3s ease 0s, border-radius 0.3s ease 0s, box-shadow 0.3s ease 0s, -webkit-border-radius 0.3s ease 0s, -webkit-box-shadow 0.3s ease 0s;">
                                        <div class="elementor-image"
                                            style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box;">
                                            <img width="200" height="200"
                                                src="http://medlifeao.md/wp-content/uploads/2018/11/alogen.jpg"
                                                class="attachment-full size-full" alt=""
                                                srcset="http://medlifeao.md/wp-content/uploads/2018/11/alogen.jpg 200w, http://medlifeao.md/wp-content/uploads/2018/11/alogen-150x150.jpg 150w"
                                                sizes="(max-width: 200px) 100vw, 200px"
                                                style="background: transparent; border-width: initial; border-color: initial; border-image: initial; margin: 0px; padding: 0px; max-width: 100%; line-height: 0; height: auto; box-sizing: border-box; border-radius: 0px; box-shadow: none; display: inline-block;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-id="19f417d"
                        class="elementor-element elementor-element-19f417d elementor-column elementor-col-16 elementor-top-column"
                        data-element_type="column"
                        style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; position: relative; min-height: 1px; display: flex; width: 189.984px;">
                        <div class="elementor-column-wrap elementor-element-populated"
                            style="background: transparent; border: 0px; margin: 0px; padding: 10px; vertical-align: baseline; box-sizing: border-box; display: flex; width: 189.984px; position: relative;">
                            <div class="elementor-widget-wrap"
                                style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; width: 169.984px; position: relative;">
                                <div data-id="d72d8e2"
                                    class="elementor-element elementor-element-d72d8e2 elementor-widget elementor-widget-image"
                                    data-element_type="image.default"
                                    style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; position: relative; text-align: center;">
                                    <div class="elementor-widget-container"
                                        style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; transition: background 0.3s ease 0s, border 0.3s ease 0s, border-radius 0.3s ease 0s, box-shadow 0.3s ease 0s, -webkit-border-radius 0.3s ease 0s, -webkit-box-shadow 0.3s ease 0s;">
                                        <div class="elementor-image"
                                            style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box;">
                                            <img width="200" height="200"
                                                src="http://medlifeao.md/wp-content/uploads/2018/11/tehnomag.jpg"
                                                class="attachment-full size-full" alt=""
                                                srcset="http://medlifeao.md/wp-content/uploads/2018/11/tehnomag.jpg 200w, http://medlifeao.md/wp-content/uploads/2018/11/tehnomag-150x150.jpg 150w"
                                                sizes="(max-width: 200px) 100vw, 200px"
                                                style="background: transparent; border-width: initial; border-color: initial; border-image: initial; margin: 0px; padding: 0px; max-width: 100%; line-height: 0; height: auto; box-sizing: border-box; border-radius: 0px; box-shadow: none; display: inline-block;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </h4>
    <h1 class="elementor-heading-title elementor-size-xl"
        style="background: transparent; border: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; vertical-align: baseline; font-weight: 600; font-family: Roboto, sans-serif; color: rgb(38, 102, 114); line-height: 1; font-size: 39px; box-sizing: border-box;">
        Centru Medicilor de Familie din municipiul Balți</h1>
    <p
        style="background: transparent; border: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; vertical-align: baseline; font-weight: 600; font-family: Roboto, sans-serif; color: rgb(38, 102, 114); line-height: 1; font-size: 39px; box-sizing: border-box;">
        <br></p><img width="648" height="486" src="http://medlifeao.md/wp-content/uploads/2018/11/Foto-CMF-Balti.jpg"
        class="attachment-full size-full" alt=""
        srcset="http://medlifeao.md/wp-content/uploads/2018/11/Foto-CMF-Balti.jpg 648w, http://medlifeao.md/wp-content/uploads/2018/11/Foto-CMF-Balti-300x225.jpg 300w"
        style="box-sizing: border-box; border-width: initial; border-color: initial; border-image: initial; float: left; width: 40%;"
        open="" sans",="" arial,="" helvetica,="" sans-serif;="" font-size:="" 14px;="" text-align:="" center;=""
        background:="" transparent;="" margin:="" 0px;="" padding:="" max-width:="" 100%;="" line-height:="" 0;=""
        border-radius:="" box-shadow:="" none;="" display:="" inline-block;="" float:="" left;="" width:="" 50%;"="">
    <h4
        style="background: transparent; border: 0px; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; vertical-align: baseline; font-family: Raleway; color: rgb(0, 0, 0); line-height: 1.2; font-size: 1.3em; box-sizing: border-box;">
        &nbsp;Începînd cu anul 2012 AO Medlife cooperează fructuos cu&nbsp; &nbsp;Centrul Medicinii de&nbsp;
        &nbsp;Familie din mun. Bălți în furnizarea&nbsp; &nbsp;serviciilor de îngrijiri comunitare și paliative&nbsp;
        &nbsp;la domiciliu.&nbsp; &nbsp;Administrația CMF mun. Bălți este cointeresată de extinderea&nbsp; &nbsp;acestui
        tip de serviciu medical pentru populație și prin&nbsp; &nbsp;acțiunile sale, contribuie&nbsp; &nbsp;activ la
        creșterea volumului&nbsp; &nbsp;îngrijirilor medicale la domiciliu în teritoriul deservit.<br
            style="box-sizing: border-box;">&nbsp;Interacțiunea se desfășoară la toate nivelele de medicină&nbsp;
        &nbsp;primară a mun. Bălți, de&nbsp; &nbsp;la coordonare cu conducerea IMSP&nbsp; &nbsp;CMF mun. Bălți,
        continuând la nivelul&nbsp; &nbsp;Centrelor de sănătate,&nbsp; &nbsp;șefi de secție și terminând cu medicii de
        familie și zonele&nbsp; &nbsp;rurale, care aparțin mun. Bălți. Un loc important în implementarea
        îngrijirilor&nbsp; &nbsp;medicale și paliative aparține asistentelor medicale din CMF mun. Bălți.<br
            style="box-sizing: border-box;">&nbsp;La fiecare sector, împreună cu medicii de familie, conform
        indicațiilor medicale, se&nbsp; &nbsp;efectuează selectarea pacienților, care au nevoie de acest tip de
        servicii, procedurile&nbsp; &nbsp;necesare și volumul individual de îngrijiri pentru fiecare pacient. Anual,
        liste de&nbsp; &nbsp;pacienți care au nevoie de servicii de îngrijire la domiciliu sunt discutate cu&nbsp;
        &nbsp;conducătorii Centrului Medicilor de Familie mun. Bălți, actualizate și extinse. Este&nbsp; &nbsp;oformată
        documentația medicală corespunzătoare.<br style="box-sizing: border-box;">&nbsp;Rezultatul multor ani de
        activitate comună a dus la o creștere semnificativă a&nbsp; &nbsp;volumului serviciilor de îngrijiri medicale
        furnizate la domiciliu. În același timp,&nbsp; &nbsp;aria de acoperire a acestui serviciu s-a extins. Unul
        dintre principalele rezultate ale&nbsp; &nbsp;colaborării active este că teritoriul deservit de medicina primară
        mun. Bălți astăzi&nbsp; &nbsp;este cel mai mult acoperit cu servicii pentru îngrijirii comunitare și paliative
        la domiciliu în Republica Moldova.<br style="box-sizing: border-box;">Acest rezultat are un efect pozitiv asupra
        calității vieții beneficiarilor acestui tip de serviciu – persoanele în vârstă, persoane cu mai multe boli
        cronice în stadiul avansat, țintuiți la pat, pacienții cu cancer și alte boli incurabile.</h4>
    <h4
        style="text-align: justify; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow-wrap: break-word; vertical-align: baseline;">
        <section data-id="3dc64a0"
            class="elementor-element elementor-element-3dc64a0 elementor-section-stretched elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
            data-settings="{" stretch_section":"section-stretched"}"="" data-element_type="section"
            style="box-sizing: border-box; position: relative; width: 1440px; font-family: " open="" sans",="" arial,=""
            helvetica,="" sans-serif;="" font-size:="" 14px;="" text-align:="" start;="" left:="" -130px;"="">
            <div class="elementor-container elementor-column-gap-default"
                style="background: transparent; border: 0px; margin: 0px auto; padding: 0px; vertical-align: baseline; box-sizing: border-box; display: flex; position: relative;">
                <div class="elementor-row"
                    style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; width: 1440px; display: flex;">
                    <div data-id="d18a79f"
                        class="elementor-element elementor-element-d18a79f elementor-column elementor-col-50 elementor-top-column"
                        data-element_type="column"
                        style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; position: relative; min-height: 1px; display: flex; width: 766.359px;">
                        <div class="elementor-column-wrap elementor-element-populated"
                            style="background: transparent; border: 0px; margin: 0px; padding: 10px; vertical-align: baseline; box-sizing: border-box; display: flex; width: 766.359px; position: relative;">
                            <div class="elementor-widget-wrap"
                                style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; width: 746.359px; position: relative;">
                                <div data-id="cf6859e"
                                    class="elementor-element elementor-element-cf6859e elementor-widget elementor-widget-text-editor"
                                    data-element_type="text-editor.default"
                                    style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; position: relative; font-family: Roboto, sans-serif;">
                                    <div class="elementor-widget-container"
                                        style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; transition: background 0.3s ease 0s, border 0.3s ease 0s, border-radius 0.3s ease 0s, box-shadow 0.3s ease 0s, -webkit-border-radius 0.3s ease 0s, -webkit-box-shadow 0.3s ease 0s;">
                                        <div class="elementor-text-editor elementor-clearfix"
                                            style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </h4>
    <h1 class="elementor-heading-title elementor-size-xl"
        style="background: transparent; border: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; vertical-align: baseline; font-weight: 600; font-family: Roboto, sans-serif; color: rgb(38, 102, 114); line-height: 1; font-size: 39px; box-sizing: border-box;">
        Colaborarea Cu Colegiul de Medicina Balti</h1>
    <p
        style="background: transparent; border: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; vertical-align: baseline; font-weight: 600; font-family: Roboto, sans-serif; color: rgb(38, 102, 114); line-height: 1; font-size: 39px; box-sizing: border-box;">
        <br></p><img width="640" height="423" src="http://medlifeao.md/wp-content/uploads/2018/11/CMB-1.jpg"
        class="attachment-large size-large" alt=""
        srcset="http://medlifeao.md/wp-content/uploads/2018/11/CMB-1.jpg 960w, http://medlifeao.md/wp-content/uploads/2018/11/CMB-1-300x198.jpg 300w, http://medlifeao.md/wp-content/uploads/2018/11/CMB-1-768x508.jpg 768w"
        sizes="(max-width: 640px) 100vw, 640px"
        style="box-sizing: border-box; border-width: initial; border-color: initial; border-image: initial; font-size: 24px; text-align: center; background: transparent; margin: 0px; padding: 0px; max-width: 100%; line-height: 0; height: auto; border-radius: 0px; box-shadow: none; display: inline-block; float: right;"><span
        style="background-color: transparent; font-family: Raleway; font-size: 1.3em; color: inherit;">AO Medlife
        colaborează cu Colegiul de Medicină Bălți cu scopul de a pune în aplicare îngrijirile medicale comunitare și
        paliative la domiciliu. În 2018 a fost încheiat un contract de colaborare între Colegiul de Medicină Bălti și AO
        Medlife. AO Medlife a oferit elevilor Colegiului baza, structura și abilitățile dobândite ale organizației în
        implementarea ingrijirilor comunitare și paliative la domiciliu. Studenții fac studii practice pe baza AO
        Medlife, unde își consolidează cunoștințele teoretice dobândite și obțin abilități practice în îngrijiri
        medicale la domiciliu.</span>
    <h4
        style="background: transparent; border: 0px; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; vertical-align: baseline; font-family: Raleway; color: rgb(0, 0, 0); line-height: 1.2; font-size: 1.3em; box-sizing: border-box;">
        &nbsp; O componentă importantă a cooperării este vizita pacienților dependenți de către asistenta medicală AO
        Medlife împreuna cu studenții la domiciliu pe bază voluntară și nerambursabilă.</h4>
    <h4
        style="background: transparent; border: 0px; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; vertical-align: baseline; font-family: Raleway; color: rgb(0, 0, 0); line-height: 1.2; font-size: 1.3em; box-sizing: border-box;">
        &nbsp; 82 de voluntari participă la vizitele la domiciliu a persoanelor vârstnice și la &nbsp;pacienți care au
        nevoie de îngrijiri, efectuează diverse manopere, participă la viața persoanelor singure. Această activitate
        îmbunătățește semnificativ calitatea vieții pacienților severi și netratabili, atât din punct de vedere medical,
        cât și din punct de vedere social.</h4>
    <h4
        style="text-align: justify; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; overflow-wrap: break-word; vertical-align: baseline;">
        <section data-id="efc82ac"
            class="elementor-element elementor-element-efc82ac elementor-section-stretched elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
            data-settings="{" stretch_section":"section-stretched"}"="" data-element_type="section"
            style="box-sizing: border-box; position: relative; width: 1440px; font-family: " open="" sans",="" arial,=""
            helvetica,="" sans-serif;="" font-size:="" 14px;="" text-align:="" start;="" left:="" -130px;"="">
            <div class="elementor-container elementor-column-gap-default"
                style="background: transparent; border: 0px; margin: 0px auto; padding: 0px; vertical-align: baseline; box-sizing: border-box; display: flex; position: relative;">
                <div class="elementor-row"
                    style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; width: 1440px; display: flex;">
                    <div data-id="d5ba009"
                        class="elementor-element elementor-element-d5ba009 elementor-column elementor-col-50 elementor-top-column"
                        data-element_type="column"
                        style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; position: relative; min-height: 1px; display: flex; width: 637.828px;">
                        <div class="elementor-column-wrap elementor-element-populated"
                            style="background: transparent; border: 0px; margin: 0px; padding: 10px; vertical-align: baseline; box-sizing: border-box; display: flex; width: 637.828px; position: relative;">
                            <div class="elementor-widget-wrap"
                                style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; width: 617.828px; position: relative;">
                                <div data-id="75f7056"
                                    class="elementor-element elementor-element-75f7056 elementor-widget elementor-widget-text-editor"
                                    data-element_type="text-editor.default"
                                    style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; position: relative; font-family: Roboto, sans-serif;">
                                    <div class="elementor-widget-container"
                                        style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; transition: background 0.3s ease 0s, border 0.3s ease 0s, border-radius 0.3s ease 0s, box-shadow 0.3s ease 0s, -webkit-border-radius 0.3s ease 0s, -webkit-box-shadow 0.3s ease 0s;">
                                        <div class="elementor-text-editor elementor-clearfix"
                                            style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-id="e6b6f97"
                        class="elementor-element elementor-element-e6b6f97 elementor-column elementor-col-50 elementor-top-column"
                        data-element_type="column"
                        style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; position: relative; min-height: 1px; display: flex; width: 802.156px;">
                        <div class="elementor-column-wrap elementor-element-populated"
                            style="background: transparent; border: 0px; margin: 0px; padding: 10px; vertical-align: baseline; box-sizing: border-box; display: flex; width: 802.156px; position: relative;">
                            <div class="elementor-widget-wrap"
                                style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; width: 782.156px; position: relative;">
                                <div data-id="a79ca3c"
                                    class="elementor-element elementor-element-a79ca3c elementor-widget elementor-widget-image"
                                    data-element_type="image.default"
                                    style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; position: relative; text-align: center;">
                                    <div class="elementor-widget-container"
                                        style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; transition: background 0.3s ease 0s, border 0.3s ease 0s, border-radius 0.3s ease 0s, box-shadow 0.3s ease 0s, -webkit-border-radius 0.3s ease 0s, -webkit-box-shadow 0.3s ease 0s;">
                                        <p
                                            style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box;">
                                            <br></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </h4>
    <h1 class="elementor-heading-title elementor-size-xl"
        style="background: transparent; border: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; vertical-align: baseline; font-weight: 600; font-family: Roboto, sans-serif; color: rgb(38, 102, 114); line-height: 1; font-size: 39px; box-sizing: border-box;">
        SA Labormed Phara</h1>
    <h4
        style="background: transparent; border: 0px; margin: 0px 0px 20px; padding: 0px; vertical-align: baseline; font-weight: normal; font-family: Raleway; color: rgb(0, 0, 0); line-height: 1.2; font-size: 1.3em; box-sizing: border-box; text-align: center;">
        <strong
            style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; font-weight: bold; box-sizing: border-box;"><span
                style="background: transparent; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box; color: rgb(0, 0, 0);"><img
                    src="http://medlifeao.md/wp-content/uploads/2018/11/Alogen-RO-Office-1.jpg"
                    style="float: right;"><br style="box-sizing: border-box;">
                <p style="text-align: left;"><span
                        style="background: transparent; font-weight: normal; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box;">SA
                        Labormed Pharma colaborează cu AO Medlife în domeniul susținerii serviciilor de îngrijiri
                        medicale și paliative la domiciliu în Republica Moldova. Această colaborare este orientată spre
                        ocrotirea sănătății populației și dezvoltarea Asociației ca instituție.</span><span
                        style="background: transparent; font-size: 1.3em; font-weight: normal; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box;">AO
                        „Medlife” este o organizaţie nonprofit, care funcţionează conform &nbsp;Programului Unic RM în
                        domeniul ocrotirii sănătăţii, și este finanțată pentru serviciile de îngrijiri la domiciliu din
                        fondurilor CNAM. În acest context, reieșind din faptul că resursele financiare ale Asociației
                        sunt limitate, pentru deservirea calitativă și ameliorarea stării pacienților, este necesar un
                        suport financiar suplimentar. Cheltuielile adăugătoare sunt redirecționate pentru serviciile
                        transport, care sunt necesare deplasării personalului la domiciliul pacienților, motivarea
                        lucrătorilor medicali și procurarea medicamentelor destinate, conform indicațiilor medicale
                        beneficiarilor de îngrijiri la domiciliu – pacienţi de vîrstă senilă, invalizii de gr. I-II,
                        persoanele ţintite la pat, cu patologii cronice multiple în stadiu avansat.</span><span
                        style="background: transparent; font-size: 1.3em; font-weight: normal; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box;">SA
                        Labormed Pharma acordă ajutor financiar, care integral este orientat spre beneficiul pacienților
                        și ameliorarea stării lor de sănătate și contribuie la dezvoltarea Asociației ca
                        instituția.</span><span
                        style="background: transparent; font-size: 1.3em; font-weight: normal; border: 0px; margin: 0px; padding: 0px; vertical-align: baseline; box-sizing: border-box;">Prin
                        acest gest nobil &nbsp;se manifestă o responsabilitate socială faţă de ei, iar poziţia ocupată
                        este şi o parte importantă a imaginii instituţii SA Labormed Pharma.&nbsp;</span></p>
            </span></strong></h4>
</div>
