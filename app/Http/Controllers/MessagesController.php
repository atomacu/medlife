<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class MessagesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $to_name = 'Medlife email';
        $to_email = env('MAIL_TO');
       
        Mail::send('emails.mail', ['name' => $_POST["name"], 'email' => $_POST["email"], 'msg' => $_POST["message"]], function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name." ".(string)$_POST["name"])
            ->subject($to_name." ".(string)$_POST["name"]);
            $message->from('valeracernetchi@gmail.com', $to_name);
        });
        return redirect()->back();
    }
}
